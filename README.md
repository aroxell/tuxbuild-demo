# 5 Minute Build Automation with TuxBuild

In 5 minutes, you can have GitLab and TuxBuild automatically validating your
kernel tree.

Prerequisites:
- TuxBuild token
- GitLab account
- Public Linux kernel git repository

## Set up GitLab repository

1. Log into GitLab.com.
2. Fork this repository.
3. Edit [.gitlab.yml](.gitlab.yml) and change `--git-repo` and `--git-ref` to point to
your branch.
4. Edit [builds.yml](build.yml) and define your own set of builds.
5. Navigate to Settings->CI/CD->Variables and add your TUXBUILD_TOKEN
![Set TuxBuild Token](images/set_tuxbuild_token.png)
6. Navigate to Settings->CI/CD->General pipelines: Timeout: and set it to 2h
![Set Timeout](images/set_pipeline_timeout.png)
7. Navigate to Settings->CI/CD->Pipeline triggers and create a trigger so that you can use curl to trigger your build.
![Create Pipeline Trigger](images/create_pipeline_trigger.png)
Save the curl command into a shell script or as a shell alias. For example:
```
alias trigger-build="curl -X POST -F token=e44cdbf4e7cdccab2a0455ae865209 -F ref=master https://gitlab.com/api/v4/projects/16514226/trigger/pipeline"
```
8. Finally, click on the bell to set up gitlab notifications. You can receive notifications for all runs, or just failures.

Your pipelines can be seen at [CD/CD->Pipelines](pipelines/)

## FAQ

### How can I trigger builds automatically when pushed?
